class GroupController < ApplicationController
	def list
	  @groups = Group.all
	end

	def show
	  @group = Group.find(params[:id])
	end

	def new
	  @group = Group.new
	end

	

	def create
	  @group = Groupr.new(group_params)

	  if @group.save
	     redirect_to :action => 'list'
	  else
	     render :action => 'new'
	  end
	end

	private
	def group_params
	  params.require(:groups).permit(:name, :description, :cover_picture, :access_by, :creator_id)
	end
end
