class InviteController < ApplicationController
	def list
	  @invites = Invite.all
	end

	def show
	  @invite = Invite.find(params[:id])
	end
end
