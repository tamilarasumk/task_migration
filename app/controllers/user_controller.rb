class UserController < ApplicationController
	def list
	  @users = User.all
	end

	def show
	  @user = User.find(params[:id])
	end

	def new
	  @user = User.new
	end

	def user_params
	  params.require(:users).permit(:first_name, :last_name, :email, :password_hash, :date_of_birth, :profile_picture, :cover_picture)
	end

	def create
	  @user = User.new(user_params)

	  if @user.save
	     redirect_to :action => 'list'
	  else
	     render :action => 'new'
	  end
	end
end
