class Group < ApplicationRecord

	has_many :members
	belongs_to :creator, :class_name => 'User'

	validates :name, presence: true, length:{maximum:30}
	validates :access_by, presence: true
	# validates :creator_id, presence: true
end
