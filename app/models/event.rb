class Event < ApplicationRecord

	belongs_to :hoster_id, :class_name => 'User'
	belongs_to :group

	validates :name, presence: true
	validates :description, presence: true
	validates :venue, presence: true
	validates :guests, presence: true
	validates_uniqueness_of :user_id, scope: :name 
end
