class Comment < ApplicationRecord

	belongs_to :user

	validates :description, presence: true
	validates :posted_at, presence: true
end
