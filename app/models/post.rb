class Post < ApplicationRecord

	belongs_to :group
	has_many :likes
	has_many :comments
	belongs_to :hoster, :class_name => 'User'

	Share = ["friends", "friends of friends", "public"]

	validates :posted_at, presence: true
	validates :post_link, presence: true
	validates :shared_with, presence: true, inclusion:{in:Share}
	validates :thumbnail, presence: true
	validates_uniqueness_of :user_id
	validates :hoster_id, presence: true
end
