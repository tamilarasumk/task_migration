class User < ApplicationRecord

	has_many :created_groups, :class_name => 'Group', :foreign_key => 'creator_id'
	has_many :created_posts, :class_name => 'Post', :foreign_key => 'hoster_id'

	Mail_Reg_Ex = /\A[a-z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\Z/i
	Pass_Reg_Ex = /\A(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}\Z/i

	validates :first_name, presence: true, length: {maximum: 30}
	validates :last_name, presence: true, length: {maximum: 30}
	validates_uniqueness_of :email, presence: true, format: Mail_Reg_Ex
	validates :date_of_birth, presence: true
	validates :password_hash, presence: true, format: Pass_Reg_Ex

end