class Member < ApplicationRecord

	belongs_to :group

	validates :admin, presence: true
	validates_uniqueness_of :user_id, scope: :group_id
	validates :group_id, presence:true
end
