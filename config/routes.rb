Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

   get 'user/list'
   get 'user/new'
   post 'user/create'
   get 'user/show'

   get 'group/list'
   get 'group/new'
   post 'group/create'
   get 'group/show'   
end