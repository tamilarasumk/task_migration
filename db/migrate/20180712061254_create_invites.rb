class CreateInvites < ActiveRecord::Migration[5.2]
  def change
    create_table :invites do |t|
      t.references :user, index: true, foreign_key: true, null: false
      t.references :member, index: true, foreign_key: true, null: false
      t.timestamp :invited_at, null: false
      t.string :email, null: false
      t.boolean :status, null: false
      t.references :group, foreign_key: true, null: false
      t.timestamps
    end
  end
end