class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.timestamp :posted_at, null: false
      t.string :post_link, null: false
      t.string :shared_with, null: false
      t.string :thumbnail, null: false
      t.string :description, null: false
      t.integer :shares, null: false
      t.references :hoster, unique: true, null: false
      t.references :group, foreign_key: true, null: false
      t.timestamps
    end
  end
end