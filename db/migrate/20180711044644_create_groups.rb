class CreateGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :groups do |t|
      t.string :name, null: false
      t.string :description, null: false
      t.string :cover_pic, null: false
      t.boolean :access_by, null: false
      t.references :creator, index: true, null: false
      t.timestamps
    end

  end
end