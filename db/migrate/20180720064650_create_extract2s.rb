class CreateExtract2s < ActiveRecord::Migration[5.2]
  def change
    create_table :extract2s do |t|
      t.string :email, null: false, index: true
      t.integer :roll_no, null: false, index: true
      t.string :name, null: false
      t.string :guide, null: false
      t.string :organization, null: false
      t.string :title, null: false
      t.string :branch, null: false

      t.timestamps
    end
  end
end
