class CreateMembers < ActiveRecord::Migration[5.2]
  def change
    create_table :members do |t|
      t.boolean :admin, null: false
      t.references :user, foreign_key: true, null: false
      t.references :group, foreign_key: true, null: false
      t.timestamps
    end
    add_index :members, [:user_id, :group_id], unique: true
  end
end
