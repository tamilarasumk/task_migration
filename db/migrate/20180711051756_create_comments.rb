class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.string :description, null: false
      t.timestamp :posted_at, null: false
      t.references :member, index: true, foreign_key: true, null: false
      t.references :post, index: true, foreign_key: true, null: false
      t.references :user, index: true, foreign_key: true, null: false
      t.timestamps
    end
  end
end