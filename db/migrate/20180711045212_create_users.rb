class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.string :email, null: false
      t.string :password_hash, null: false
      t.timestamp :date_of_birth, null: false
      t.string :profile_picture, null: false
      t.string :cover_picture, null: false

      t.timestamps
    end
    add_index :users, :email, unique: true
  end
end
