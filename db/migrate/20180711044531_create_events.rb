class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :name, null: false
      t.string :description, null: false
      t.string :venue, null: false
      t.integer :guests_total, null: false
      t.string :event_url, null: false
      t.references :user, foreign_key: true, null: false
      t.timestamps
    end
    add_index :events, [:user_id, :name], unique: true
  end
end