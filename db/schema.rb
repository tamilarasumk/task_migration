# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_07_20_064650) do

  create_table "comments", force: :cascade do |t|
    t.string "description", null: false
    t.datetime "posted_at", null: false
    t.integer "member_id", null: false
    t.integer "post_id", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["member_id"], name: "index_comments_on_member_id"
    t.index ["post_id"], name: "index_comments_on_post_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "name", null: false
    t.string "description", null: false
    t.string "venue", null: false
    t.integer "guests_total", null: false
    t.string "event_url", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id", "name"], name: "index_events_on_user_id_and_name", unique: true
    t.index ["user_id"], name: "index_events_on_user_id"
  end

  create_table "extract2s", force: :cascade do |t|
    t.string "email", null: false
    t.integer "roll_no", null: false
    t.string "name", null: false
    t.string "guide", null: false
    t.string "organization", null: false
    t.string "title", null: false
    t.string "branch", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_extract2s_on_email"
    t.index ["roll_no"], name: "index_extract2s_on_roll_no"
  end

  create_table "extracts", force: :cascade do |t|
    t.string "email", null: false
    t.integer "roll_no", null: false
    t.string "name", null: false
    t.string "guide", null: false
    t.string "organization", null: false
    t.string "title", null: false
    t.string "branch", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_extracts_on_email"
    t.index ["roll_no"], name: "index_extracts_on_roll_no"
  end

  create_table "groups", force: :cascade do |t|
    t.string "name", null: false
    t.string "description", null: false
    t.string "cover_pic", null: false
    t.boolean "access_by", null: false
    t.integer "creator_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["creator_id"], name: "index_groups_on_creator_id"
  end

  create_table "invites", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "member_id", null: false
    t.datetime "invited_at", null: false
    t.string "email", null: false
    t.boolean "status", null: false
    t.integer "group_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_invites_on_group_id"
    t.index ["member_id"], name: "index_invites_on_member_id"
    t.index ["user_id"], name: "index_invites_on_user_id"
  end

  create_table "likes", force: :cascade do |t|
    t.integer "member_id", null: false
    t.integer "post_id", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["member_id"], name: "index_likes_on_member_id"
    t.index ["post_id"], name: "index_likes_on_post_id"
    t.index ["user_id"], name: "index_likes_on_user_id"
  end

  create_table "members", force: :cascade do |t|
    t.boolean "admin", null: false
    t.integer "user_id", null: false
    t.integer "group_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_members_on_group_id"
    t.index ["user_id", "group_id"], name: "index_members_on_user_id_and_group_id", unique: true
    t.index ["user_id"], name: "index_members_on_user_id"
  end

  create_table "posts", force: :cascade do |t|
    t.datetime "posted_at", null: false
    t.string "post_link", null: false
    t.string "shared_with", null: false
    t.string "thumbnail", null: false
    t.string "description", null: false
    t.integer "shares", null: false
    t.integer "hoster_id", null: false
    t.integer "group_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_posts_on_group_id"
    t.index ["hoster_id"], name: "index_posts_on_hoster_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name", null: false
    t.string "last_name", null: false
    t.string "email", null: false
    t.string "password_hash", null: false
    t.datetime "date_of_birth", null: false
    t.string "profile_picture", null: false
    t.string "cover_picture", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
