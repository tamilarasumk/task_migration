class GetInvites
	def print_all_invites()
		result = Hash.new
		member_hash = Hash.new
		user_hash = Hash.new
		group_hash = Hash.new

		Invite.find_in_batches(:batch_size => 100) do |invi|
			
			mem_id_from_invite = invi.collect(&:member_id).uniq
			user_id_from_invite = invi.collect(&:user_id).uniq
			group_id_from_invite = invi.collect(&:group_id).uniq

			mem_ary = Member.find(mem_id_from_invite)	
			member_hash = mem_ary.index_by(&:id) 
			user_id_ary_from_mem = mem_ary.collect(&:user_id)
			user_id_ary = (user_id_from_invite + user_id_ary_from_mem).uniq
			user_ary = User.find(user_id_ary)
			user_ary.each{|x| user_hash[x.id] = "#{x.first_name} #{x.last_name}"}

			group_ary = Group.find(group_id_from_invite)
			group_ary.each{|x| group_hash[x.id] = x.name}

			puts user_hash
			invi.each do |invitation|
				member_name = user_hash[member_hash[invitation.member_id].user_id]
				user_name = user_hash[invitation.user_id]
				group_name = group_hash[invitation.group_id]

				if result.key?(member_name)
					if result[member_name].key?(group_name)
						result[member_name][group_name] << user_name
					else
						result[member_name][group_name] = Array.new
						result[member_name][group_name] << user_name
					end
				else
					result[member_name] = {group_name.to_sym => [user_name]}
				end
			end
		end
		# result.each do |key,value|
		# 	puts "#{key} invited: "
		# 	result[key].each do |key1,value|
		# 		puts "In #{key1} : #{value}"
		# 	end
		# end
	end
end